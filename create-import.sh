#!/bin/bash
cd /var/www/html/l37/
head -10 template.xml > import.xml
for i in `cat hosts.csv`
do
        XXIP=`echo $i|awk -F";" '{ print $2 }'`
        XXHOST=`echo $i|awk -F";" '{ print $1 }'`
        XXNAME=`echo $i|awk -F";" '{ print $1 }'`
        XXversion=`echo $i|awk -F";" '{ print $3 }'`
        XXsecurityname=`echo $i|awk -F";" '{ print $4 }'`
        XXsecuritylevel=`echo $i|awk -F";" '{ print $5 }'`
        XXauthprotocol=`echo $i|awk -F";" '{ print $6 }'`
        XXauthpassphrase=`echo $i|awk -F";" '{ print $7 }'`
        XXprivpassphrase=`echo $i|awk -F";" '{ print $8 }'`
        cat host.xml | sed "s/XXIP/$XXIP/g" >> import.xml
        sed -i "s/XXHOST/$XXHOST/g" import.xml
        sed -i "s/XXNAME/$XXNAME/g" import.xml
	sed -i "s/XXsecurityname/$XXsecurityname/g" import.xml
	sed -i "s/XXsecuritylevel/$XXsecuritylevel/g" import.xml
	sed -i "s/XXauthprotocol/$XXauthprotocol/g" import.xml
	sed -i "s/XXauthpassphrase/$XXauthpassphrase/g" import.xml
	sed -i "s/XXprivpassphrase/$XXprivpassphrase/g" import.xml
	sed -i "s/XXversion/$XXversion/g" import.xml		
done
tail -3 template.xml >> import.xml
#while true ; do ./ /var/www/html/l37/create-import.sh & sleep 5; done
